﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace AutoVerzekeringsPremieTest
{
    public class PolicyHolderTest
    {
        [Theory]
        [InlineData(5, 1, 5)]
        [InlineData(8, 1, 8)]
        [InlineData(5, 7, 5)]
        [InlineData(6, 9, 6)]
        [InlineData(0, 3, 0)]
        [InlineData(0, 0, 0)]
        public void CalculateLicenseAgeByYears(int licenseAgeYears, int licenseAgeMonths, int expectedAge)
        {
            // Arrange
            string carLicenseStartDate = DateTime.Now.AddYears(-licenseAgeYears).AddMonths(-licenseAgeMonths).ToString("dd-MM-yyyy");

            // Act
            var policyHolder = new PolicyHolder(25, carLicenseStartDate, 4500, 0);

            // Assert
            Assert.Equal(expectedAge, policyHolder.LicenseAge);
        }
    }
}
