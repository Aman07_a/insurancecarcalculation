using Moq;
using System;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace AutoVerzekeringsPremieTest
{
    public class PremiumCalculationTest
    {
        private const double Period = 20.00;

        [Fact]
        public void PremiumCorrectCalculated()
        {
            // Arrange
            int powerInKw = 80;
            int valueInEuros = 6000;

            // Year = 1
            int constructionYear = DateTime.Now.Year - 1;

            // Vehicle
            Vehicle vehicle = new Vehicle(powerInKw, valueInEuros, constructionYear);

            double expectedPremium = 25;

            // Act
            double actualPremium = PremiumCalculation.CalculateBasePremium(vehicle);

            // Assert
            Assert.Equal(expectedPremium, actualPremium);
        }

        [Theory]
        [InlineData(22, 6, 25 * 1.15)]
        [InlineData(23, 6, 25)]
        [InlineData(24, 6, 25)]
        [InlineData(29, 4, 25 * 1.15)]
        [InlineData(29, 5, 25 * 1.15)]
        [InlineData(29, 6, 25)]
        public void FifteenPercentFeeIFAgeUnder23OrLicenseUnder5Year(int age, int licenseAge, double expectedPremium)
        {
            // Arrange
            Vehicle vehicle = new Vehicle(80, 6000, 2020);

            string licenseDate = DateTime.Now.AddYears(-licenseAge).ToString("dd-MM-yyyy");
            PolicyHolder policyHolder = new PolicyHolder(age, licenseDate, 4500, 0);

            InsuranceCoverage coverage = InsuranceCoverage.WA;

            // Act
            PremiumCalculation actualPremium = new PremiumCalculation(vehicle, policyHolder, coverage);

            // Assert
            Assert.Equal(expectedPremium, actualPremium.PremiumAmountPerYear);

        }

        [Theory]
        [InlineData(1000, 25 * 1.05)]
        [InlineData(3599, 25 * 1.05)]
        [InlineData(3600, 25 * 1.02)]
        [InlineData(4499, 25 * 1.02)]
        [InlineData(4500, 25)]
        public void UpdatePremiumDifferentPostalCodeWithoutFees(int postalCode, double expectedPremium)
        {
            // Arrange
            Vehicle vehicle = new Vehicle(80, 6000, 2020);
            PolicyHolder policyHolder = new PolicyHolder(29, DateTime.Now.AddYears(-6).ToString("dd-MM-yyyy"), postalCode, 0);
            InsuranceCoverage coverage = InsuranceCoverage.WA;

            // Act
            PremiumCalculation actualPremium = new PremiumCalculation(vehicle, policyHolder, coverage);

            // Assert
            Assert.Equal(expectedPremium, actualPremium.PremiumAmountPerYear);
        }

        [Theory]
        [InlineData(InsuranceCoverage.WA, 25)]
        [InlineData(InsuranceCoverage.WA_PLUS, 25 * 1.20)]
        [InlineData(InsuranceCoverage.ALL_RISK, 25 * 2.0)]
        private static void PremiumAmountIsDifferentForDifferentCoverages(InsuranceCoverage coverage, double expectedPremium)
        {
            // Arrange
            Vehicle vehicle = new Vehicle(80, 6000, 2020);
            PolicyHolder policyHolder = new PolicyHolder(29, DateTime.Now.AddYears(-6).ToString("dd-MM-yyyy"), 4500, 0);

            // Act
            PremiumCalculation actualPremium = new PremiumCalculation(vehicle, policyHolder, coverage);

            // Assert
            Assert.Equal(expectedPremium, actualPremium.PremiumAmountPerYear);
        }

        [Theory]
        [InlineData(5, 25)]
        [InlineData(6, 25 * 0.95)]
        [InlineData(17, 25 * 0.40)]
        [InlineData(21, 25 * 0.35)]
        [InlineData(23, 25 * 0.35)]
        public void PremiumAmountIsCalculatedWithDamageFreeYears(int damageFreeYears, double expectedPremium)
        {
            // Arrange
            Vehicle vehicle = new Vehicle(80, 6000, 2020);
            PolicyHolder policyHolder = new PolicyHolder(29, DateTime.Now.AddYears(-6).ToString("dd-MM-yyyy"), 4500, damageFreeYears);
            InsuranceCoverage coverage = InsuranceCoverage.WA;

            // Act
            PremiumCalculation actualPremium = new PremiumCalculation(vehicle, policyHolder, coverage);

            // Assert
            Assert.Equal(expectedPremium, actualPremium.PremiumAmountPerYear);
        }

        [Theory]
        [InlineData(PremiumCalculation.PaymentPeriod.YEAR, 25 * 1.0258)]
        [InlineData(PremiumCalculation.PaymentPeriod.MONTH, 25 / 12.0)]
        public void PremiumAmountIsCalculatedDifferentPayment(PremiumCalculation.PaymentPeriod period, double expectedResult)
        {
            // Arrange
            Vehicle vehicle = new Vehicle(80, 6000, 2020);
            PolicyHolder policyHolder = new PolicyHolder(29, DateTime.Now.AddYears(-6).ToString("dd-MM-yy"), 4500, 0);
            InsuranceCoverage coverage = InsuranceCoverage.WA;

            // Act
            PremiumCalculation actualPremiumCalculation = new PremiumCalculation(vehicle, policyHolder, coverage);
            double actualResult = actualPremiumCalculation.PremiumPaymentAmount(period);

            // Assert
            Assert.Equal(Math.Round(expectedResult, 2), actualResult);
        }

        [Fact]
        public void EndingPremiumRoundOnTwoDecimals()
        {
            // Arrange
            Vehicle vehicle = new Vehicle(80, 6000, 2020);
            PolicyHolder policyHolder = new PolicyHolder(29, DateTime.Now.AddYears(-6).ToString("dd-MM-yy"), 4500, 0);
            InsuranceCoverage coverage = InsuranceCoverage.WA;

            // Act
            var mockOptions = new Mock<PremiumCalculation>(vehicle, policyHolder, coverage);

            double actualPremiumCalculationResult1 = Convert.ToDouble(mockOptions.Object.PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.MONTH));
            double actualPremiumCalculationResult2 = Convert.ToDouble(mockOptions.Object.PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.YEAR));

            // Assert
            Assert.StrictEqual(2.08, actualPremiumCalculationResult1);
            Assert.StrictEqual(25.64, actualPremiumCalculationResult2);

        }
    }
}
