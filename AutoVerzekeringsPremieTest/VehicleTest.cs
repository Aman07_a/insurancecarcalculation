﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace AutoVerzekeringsPremieTest
{
    public class VehicleTest
    {
        [Theory]
        [InlineData(-1, 1)]
        [InlineData(0, 0)]
        [InlineData(1, 0)]
        public void CalculateAgeOfVehicleInYears(int year, int expectedAge)
        {
            // Arrange
            int constructionYear = DateTime.Now.Year + year;

            // Act
            var vehicle = new Vehicle(80, 6000, constructionYear);

            // Assert
            Assert.Equal(expectedAge, vehicle.Age);
        }


    }
}
