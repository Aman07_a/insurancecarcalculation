# README eindopdracht testen.
Naam Aman Ahmed, student nummer: 1163950.

## Deel 1.
### Wat is het doel van een unittest?  
Het doel is dat je gegevens onafhankelijk van elkaar test op een correcte werking.


### 1.1. Vehicle test. 
Het doel van de test is: of de leeftijd van het voertuig juist berekend wordt.  

Waarom heb je voor deze specifieke data van de unittest gekozen?   
2020 om te kijken, als je een afgelopen jaar invult en bekijkt of dit goed gaat.
2021 om te kijken, als je het huidige jaar invult en bekijkt of dit goed gaat. 
2022 om te kijken, als je het volgende jaar invult en bekijkt of dit goed gaat.  

Welke techniek heb je gebruikt om tot data te komen?  
Theory, want het is een basic test met 3 data variaties.    



### 1.2. PolicyHolderTest.  
Het doel van de test is: om na te gaan of de berekening van het aantal jaar dat de persoon een rijbewijs heeft juist wordt berekend.  

Waarom heb je voor deze specifieke data van de unittest gekozen?  
5 jaar en 1 maand om na te gaan of de berekening goed gaat.
8 jaar en 1 maand om na te gaan of de berekening goed gaat, als het jaar verschilt.
5 jaar en 7 maanden om na te gaan of de berekening goed gaat, als de maanden verschillen.
6 jaar en 9 maanden om na te gaan of de berekening goed gaat, als zowel de maanden als jaren verschillen.
0 jaar en 3 maanden waardoor het nog in hetzelfde jaar valt en het aantal jaar op 0 uitkomt.
0 jaar en 0 maanden zodat je eveneens checkt of het werkt op de dag van vandaag.  

Welke techniek heb je gebruikt om tot data te komen?  
Ook hier gebruik je Theory, omdat het een basic test met 6 data variaties is. 
In de Arrange is de carLicenseStartDate berekend en omgezet naar een string.
Dit heb ik gedaan door het aantal maanden en jaren aftetrekken van de datum van dit moment.    

  
### 1.3 PremiumCalculationTest.
#### 1.3.1 PremiumCorrectCalculated.    
Wat is het doel van een unittest?  
Het doel van de test is om na te gaan of de basispremie correct wordt berekend.  

Waarom heb je voor deze specifieke (data) invulling van de unittest gekozen?  
Ik heb een aantal aannemelijke waardes voor het voertuig gekozen om o.a. de basispremie te berekenen. 
* de auto is 1 jaar oud (bouwjaar 2020)
* de waarde is 6000 euro
* het vermogen is 80 kw
* de verwachte premie is 25. Hierbij is de berekening: ((6000:100-1+ 80:5):3)= 25

Welke technieken heb je gebruikt om tot deze data te komen?  
Ik heb hiervoor fact gebruikt, om de berekeningen met behulp van een aantal vaste gegevens uit te voeren.    


#### 1.3.2. FifteenPercentFeeIFAgeUnder23OrLicenseUnder5Year.  
Wat is het doel van een unittest?  
Je test of de 15% extra premie correct bij de basispremie wordt opgeteld, als een persoon 23 jaar en ouder is of als de persoon langer dan 5 jaar zijn rijbewijs heeft.  

Waarom heb je voor deze specifieke (data) invulling van de unittest gekozen?  
De variabele waarden zijn de leeftijd van de persoon en het aantal jaren dat degene zijn rijbewijs heeft.
22 jaar en 6 jaar voor het testen van de leeftijd van de persoon om grenswaarde 23 heen.
23 jaar en 6 jaar voor het testen van de leeftijd van de persoon om grenswaarde 23 heen.
24 jaar en 6 jaar voor het testen van de leeftijd van de persoon om grenswaarde 23 heen.
29 jaar en 4 jaar voor het testen van het aantal jaren dat de persoon zijn rijbewijs heeft om grenswaarde 5 heen.
29 jaar en 5 jaar voor het testen van het aantal jaren dat de persoon zijn rijbewijs heeft om grenswaarde 5 heen.
29 jaar en 6 jaar voor het testen van het aantal jaren dat de persoon zijn rijbewijs heeft om grenswaarde 5 heen.  

Welke techniek heb je gebruikt om tot data te komen?  
Ook hier gebruik je Theory, omdat het een basic test met 6 data variaties is.    


### 1.3.3. UpdatePremiumDifferentPostalCodeWithoutFees.  
Wat is het doel van een unittest?  
Je test of het updaten van de premie correct werkt, bij het optellen van de premie. Indien er sprake is van verschillende postcodes om de grenswaarden heen.  

Waarom heb je voor deze specifieke (data) invulling van de unittest gekozen?  
De variabele waarde is de postcode van de persoon.
Postcode 1000 voor het testen van grenswaarde 1000 (want dat was de minimale waarde, dus 999 kon niet). 
Postcode 3599 voor het testen van grenswaarde 3600.
Postcode 3600 voor het testen van grenswaarde 3600.
Postcode 4499 voor het testen van grenswaarde 4500.
Postcode 4500 voor het testen van grenswaarde 4500.

Welke techniek heb je gebruikt om tot data te komen?  
Ook hier gebruik je Theory, omdat het een basic test met 5 data variaties is.    


### 1.3.4. PremiumAmountIsDifferentForDifferentCoverages.  
Wat is het doel van een unittest?  
Je test of de premie correct wordt berekend wordt, als je gebruik maakt van verschillende soorten verzekeringsdekkingen.  

Waarom heb je voor deze specifieke (data) invulling van de unittest gekozen?  
De variabele is de dekking van de verzekering. Hierbij zijn er 3 varianten, namelijk: WA, WA plus en all risk.  

Welke techniek heb je gebruikt om tot data te komen?  
Ook hier gebruik je Theory, omdat het een basic test met 3 data variaties is.   


### 1.3.5. PremiumAmountIsCalculatedWithDamageFreeYears.  
Wat is het doel van een unittest?  
Je test of de premie correct wordt berekend, bij het hanteren van verschillende schadevrije jaren om de grenswaarden heen.    

Waarom heb je voor deze specifieke (data) invulling van de unittest gekozen?  
De variabele waarde is het aantal schadevrije jaren van de persoon.
5 jaar voor het testen van het ontbreken van korting.
6 jaar voor het testen van het eerste kortingspercentage van 5 %.
17 jaar voor het testen van het hebben van de waarde onder de maximale korting van 60 %.
18 jaar voor het testen van het hebben van de maximale korting van 65 %.
19 jaar voor het testen van het hebben van de maximale korting, en om te kijken of hij niet over die 65 % heen gaat.  

Welke techniek heb je gebruikt om tot data te komen?  
Hier is een Theory gebruikt voor het testen van 5 verschillende data variaties.  


### 1.3.6. PremiumAmountIsCalculatedDifferentPayment.  
Wat is het doel van een unittest?  
Je test of of de berekening van de uiteindelijke premie goed gaat, als je verschillende betalingstermijnen hanteerd.  

Waarom heb je voor deze specifieke (data) invulling van de unittest gekozen?   
De variabele waarde is de betalingstermijn van de premiebetaling.  Als je per jaar betaald dan krijg je 2,5% korting. Indien je per maand betaald dan is het jaar gedeeld door 12.
Dit zijn de enigste 2 varianten die worden aangeboden.  

Welke techniek heb je gebruikt om tot data te komen?  
Hier is een Theory gebruikt voor het testen van 2 verschillende data variaties.    

### 1.3.7. EndingPremiumRoundOnTwoDecimals.  
Wat is het doel van een unittest?  
Je test hierbij of de uiteindelijke afronding van de premie, terug naar 2 getallen achter de komma, goed gaat.  

Waarom heb je voor deze specifieke (data) invulling van de unittest gekozen?  
De premie die je moet betalen is de variabele waarde. Ik kom echter hier niet uit.  

Welke techniek heb je gebruikt om tot data te komen?    
Ik weet dat ik de Mock functie moet gebruiken om dit te testen.... maar veel geprobeerd, maar het lukt niet. 


## Deel 2.  
Beschrijf de eventuele fouten in de applicatie die je hebt gevonden tijdens het testen (wat heb je getest, wat had je verwacht en waardoor had je die verwachting?).  
Op 1 na staan alle testen op groene vinkjes. Het testen van de premie afgerond in 2 decimalen m.b.v. de Mock functie lukte mij niet.
  
### 1ste versie:
```
 var policyHolder = new PolicyHolder(25, carLicenseStartDate, 5750, 0);
```  

### Verbeterde versie:  
```
 var policyHolder = new PolicyHolder(25, carLicenseStartDate, 4500, 0);
```  
Ik had in eerste instantie een fictieve postcode genomen (in mijn geval 5750) omdat ik eerst alleen had gekeken naar verzekeringsnemer en daar de eigenschappen van.  
Omdat daar postcode 4 cijfers stond, had ik een random getal gekozen.  
Maar ik zag later dat de postcode 4500 moest zijn, omdat je anders niet correct de risico opslag van 2% kon berekenen. 

### 1ste versie:
```
[Theory]
[InlineData(PremiumCalculation.PaymentPeriod.YEAR, 25 * 1.0258)]
[InlineData(PremiumCalculation.PaymentPeriod.MONTH, 25 / 12.0)]
```  

### Verbeterde versie:  
```
[InlineData(PremiumCalculation.PaymentPeriod.YEAR, 25 * 0.975)] 
```
Als je per maand de premie betaald dan krijg je 2,5% korting dus dat zou * 0,975 zijn.  
Maar als ik de waarde 25 * 0,975 erin zet, dan geeft het een rood kruis.  
Terwijl als ik 25 * 1,025 doe dan staat die op een groen vinkje...  
Snap deze nog steeds niet.

## Deel 3.  
Ik lever de opdracht GitLab repository:  https://gitlab.com/Aman07_a/insurancecarcalculation

